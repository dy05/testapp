<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <div class="mb-4 text-sm text-gray-600">
            {{ __('Thanks for signing up! Before getting started, could you verify your email address by copying token that have been sent to your email? If you didn\'t receive the email, we will gladly send you another.') }}
        </div>

        @if (session('status') == 'verification-link-sent')
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ __('A new token has been sent to the email address you provided during registration.') }}
            </div>
        @endif

    <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('verification.notice') }}">
        @csrf

            <!-- Email Address -->
            <input type="hidden" name="email" value="{{ Auth::user()->email }}">

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full" type="text" name="password" required autofocus/>
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-button class="w-full justify-center font-bold">
                    {{ __('Validate') }}
                </x-button>
            </div>
        </form>

        <div class="mt-4 flex items-center justify-between border-t border-gray-600 pt-4">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf

                <div>
                    <button class="bg-red-700 hover:bg-red-500 inline-flex items-center px-4 py-2 rounded-md font-semibold text-xs text-white uppercase tracking-widest transition ease-linear duration-100">
                        {{ __('Resend token') }}
                    </button>
                </div>
            </form>

            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <button type="submit" class="underline text-sm text-gray-600 hover:text-gray-900">
                    {{ __('Logout') }}
                </button>
            </form>
        </div>
    </x-auth-card>
</x-guest-layout>
