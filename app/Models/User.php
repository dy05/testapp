<?php

namespace App\Models;

use App\Notifications\VerifyToken;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use App\Traits\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function setToken()
    {
        $token = strtolower(Str::random(6));

        if ($token !== 'token') {
            $this->token = encrypt($token);
            return $this->update();
        }

        return false;
    }

    public function isTokenVerified()
    {
        return decrypt($this->token) === 'token';
    }

    public function markTokenAsVerified(string $token)
    {
        if (strtolower($token) === decrypt($this->token) && strtolower($token) !== 'token') {
            $this->token = encrypt('token');
            return $this->update();
        }

        return false;
    }

    public function sendToken()
    {
        if ($this->setToken()) {
            $this->notify(new VerifyToken);
        }
    }
}
