<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\HtmlString;

class VerifyToken extends Notification
{
    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return $this->buildMailMessage(decrypt($notifiable->token));
    }

    /**
     * Get the verify token notification mail message for the Auth user.
     *
     * @param  string  $token
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    protected function buildMailMessage($token)
    {
        return (new MailMessage)
            ->subject(Lang::get('Verify Token'))
            ->line(new HtmlString(Lang::get('Enter the code to authenticate your account :').' <strong>'.strtoupper($token).'</strong>'))
            ->line('')
            ->line(Lang::get('If you did not trying to connect to your account, no further action is required.'));
    }

}
