<?php

namespace App\Actions\Fortify;

use App\Traits\TwoFactorAuthenticatable;
use Laravel\Fortify\Actions\RedirectIfTwoFactorAuthenticatable as DefaultActionRedirectIfTwoFactorAuthenticatable;

class RedirectIfTwoFactorAuthenticatable extends DefaultActionRedirectIfTwoFactorAuthenticatable
{
    public function handle($request, $next)
    {
        $user = $this->validateCredentials($request);
        $user->sendToken();

        if (optional($user)->token &&
            in_array(TwoFactorAuthenticatable::class, class_uses_recursive($user))) {
            return $this->twoFactorChallengeResponse($request, $user);
        }

        return $next($request);
    }
}

