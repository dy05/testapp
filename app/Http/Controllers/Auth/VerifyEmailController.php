<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\VerifyRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;

class VerifyEmailController extends Controller
{
    public function view(Request $request)
    {
        return $request->user()->isTokenVerified()
            ? redirect()->intended(RouteServiceProvider::HOME)
            : view('auth.verify-token');
    }

    public function verify(VerifyRequest $request)
    {
        $request->validate(['password' => 'required|string']);

        $request->verify($request->input('password'));

        event(new Verified($request->user()));

        return redirect()->intended(RouteServiceProvider::HOME.'?verified=1');
    }

    public function send(VerifyRequest $request)
    {
        $request->send();

        $request->user()->sentToken();

        return back()->with('status', 'verification-link-sent');
    }
}
