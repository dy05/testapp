<?php

namespace App\Http\Requests;

use Laravel\Fortify\Contracts\TwoFactorAuthenticationProvider;
use Laravel\Fortify\Http\Requests\TwoFactorLoginRequest as DefaultRequestTwoFactorLoginRequest;

class TwoFactorLoginRequest extends DefaultRequestTwoFactorLoginRequest
{
    public function hasValidCode()
    {
        return $this->challengedUser()->markTokenAsVerified($this->code);
    }
}
