<?php

namespace App\Http\Requests\Auth;

use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class VerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function send()
    {
        $this->ensureIsNotRateLimited(true);


        if ($this->user()->isTokenVerified()) {
            RateLimiter::clear($this->throttleSendKey());
            return redirect()->intended(RouteServiceProvider::HOME);
        }

        RateLimiter::hit($this->throttleSendKey());
    }

    /**
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function verify($token)
    {
        $this->ensureIsNotRateLimited();

        if ($token === 'token' || ! $this->user()->markTokenAsVerified(strtolower($token))) {
            RateLimiter::hit($this->throttleKey());

            throw ValidationException::withMessages([
                'token' => __('Token invalide'),
            ]);
        }

        RateLimiter::clear($this->throttleKey());
    }

    /**
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function ensureIsNotRateLimited($isSend = false)
    {
        if (! RateLimiter::tooManyAttempts($isSend ? $this->throttleSendKey() : $this->throttleKey(), 2)) {
            return;
        }

        event(new Lockout($this));

        $seconds = RateLimiter::availableIn($isSend ? $this->throttleSendKey() : $this->throttleKey());

        throw ValidationException::withMessages([
            $isSend ? 'reset' : 'token' => trans($isSend ? 'auth.reset' : 'auth.token', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ]),
        ]);
    }

    public function throttleKey()
    {
        return Str::lower($this->user()->email).'|'.$this->ip();
    }

    public function throttleSendKey()
    {
        return Str::lower($this->user()->email).'_send|'.$this->ip();
    }
}
