<?php

namespace App\Events;

use Illuminate\Auth\Events\Registered;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendTokenNotification
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public function handle(Registered $event)
    {
        if (! $event->user->isTokenVerified()) {
            $event->user->setToken();
        }
    }

}
